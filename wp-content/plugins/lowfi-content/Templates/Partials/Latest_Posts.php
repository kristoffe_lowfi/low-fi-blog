<?php
use Lowfi\Content\Main;
?>

<section class="latest-posts">

	<div class="content">

		<?php foreach ( $posts as $index => $post ) : ?>

			<article class="post">

				<?php if ( ! empty( $post['image'] ) ) : ?>
					<a href="<?php echo $post['permalink']; ?>">
						<img src="<?php echo $post['image']; ?>" alt="<?php echo $post['title']; ?>" class="featured-image" />
					</a>
	            <?php endif; ?>

				<div class="text">

					<?php echo $post['category']; ?>

					<div class="title">
						<a href="<?php echo $post['permalink']; ?>">
							<h3>
								<?php echo $post['title']; ?>
							</h3>
						</a>
	                </div>

	                <div class="excerpt">
	                    <a href="<?php echo $post['permalink']; ?>">
							<?php echo wpautop( $post['excerpt'] ); ?>
						</a>
	                </div>

				</div>

    		</article>

		<?php endforeach; ?>

    </div>

</section>
