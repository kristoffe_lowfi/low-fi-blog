<section class="latest-posts">

    <div class="inner-grid">

        <?php if ( have_posts() ) : ?>

    		<header class="page-header">
    			<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'shape' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
    		</header>

    		<div class="content">

    			<?php while ( have_posts() ) : the_post(); ?>

    				<article class="post">

    				    <a href="<?php echo get_permalink(); ?>">
    						<img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'grid-normal' ); ?>" alt="<?php the_title(); ?>" class="featured-image" />
    					</a>

    	                <div class="title">
    	                    <h3>
    							<a href="<?php echo get_permalink(); ?>">
    								<?php the_title(); ?>
    							</a>
    						</h3>
    	                </div>

    	                <div class="excerpt">
    	                    <?php echo wpautop( get_the_excerpt() ); ?>
    	                </div>

    	        	</article>

    	        <?php endwhile; ?>

    		</div>

        <?php endif; ?>

    </div>

</section>
