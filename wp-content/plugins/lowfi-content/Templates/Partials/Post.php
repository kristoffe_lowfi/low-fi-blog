<div class="inner-grid">

	<h1><?php echo $title; ?></h1>

	<div class="date">
		<?php echo $date; ?>
	</div>

	<img src="<?php echo $image_url; ?>" alt="<?php echo $title; ?>" class="featured-image" />

	<div class="content">
		<?php echo $content; ?>
	</div>

</div>
