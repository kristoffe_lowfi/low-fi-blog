<section class="cta">

    <div class="image" style="background-image: url(<?php echo $image; ?>)">
    </div>

    <div class="content">
        <div class="headline">
            <h2><?php echo $headline; ?></h2>
        </div>

        <?php if ( $subheadline !== '' ) : ?>
            <div class="subheadline">
                <?php echo $subheadline; ?>
            </div>
        <?php endif; ?>

        <?php if ( $cta_target !== '' ) : ?>
            <div class="cta">
                <a href="<?php echo get_permalink( $cta_target ); ?>" class="btn"><?php echo $cta_label; ?></a>
            </div>
        <?php endif; ?>
    </div>

</section>
