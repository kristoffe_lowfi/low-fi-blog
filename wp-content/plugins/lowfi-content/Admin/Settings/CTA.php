<?php

namespace Lowfi\Content\Admin\Settings;

use Lowfi\Content\Main;

class CTA extends Settings_Page {

	const PREFIX = 'cta_';

	public function __construct() {
		parent::__construct( self::PREFIX, 'Main call-to-action' );
	}

	function add_meta_box() {
		$meta_box = $this->enqueue_and_create_meta_box();

		$meta_box->add_field( [
			'name'            => __( 'Headline', Main::TEXT_DOMAIN ),
			'id'              => 'headline',
			'type'            => 'text',
		] );

		$meta_box->add_field( [
			'name'            => __( 'Sub-headline', Main::TEXT_DOMAIN ),
			'id'              => 'subheadline',
			'type'            => 'text',
		] );

		$meta_box->add_field( [
			'name'            => __( 'Background Image', Main::TEXT_DOMAIN ),
			'id'              => 'image',
			'type'            => 'file',
		] );

		$meta_box->add_field( [
			'name'            => __( 'CTA Label', Main::TEXT_DOMAIN ),
			'id'              => 'cta_label',
			'type'            => 'text_medium',
		] );

		$meta_box->add_field( [
			'name'            => __( 'CTA Target', Main::TEXT_DOMAIN ),
			'id'              => 'cta_target',
			'type'            => 'post_search_text',
			'post_type'       => [ 'post', 'page' ],
			'select_type'     => 'radio',
			'select_behavior' => 'replace',
		] );

	}
}
