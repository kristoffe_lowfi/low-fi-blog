<?php

namespace Lowfi\Content\Admin;

class Bootstrap {

	public function __construct() {
		$this->init();
	}

	/**
	 * Run core bootstrap hooks.
	 */
	public function init() {
		// new Settings\CTA();

		$type = new Type\Bootstrap();
		$type->init();
	}
}
