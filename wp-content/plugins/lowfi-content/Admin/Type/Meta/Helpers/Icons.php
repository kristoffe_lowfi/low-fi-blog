<?php

namespace Lowfi\Content\Admin\Type\Meta\Helpers;

use Lowfi\Content\Main;


class Icons {

	public static function get_select_options( $selected ) {
		$icons = self::get_options();

		$options = '';
		foreach ( $icons as $index => $value ) {
			$is_selected = ( $selected === $index ) ? 'selected' : '';

			$options .= sprintf(
				'<option value="%s" %s>%s</option>',
				$index,
				$is_selected,
				$value
			);
		}

		return $options;
	}

	public static function get_options() {
		$icons = [
			'icon-facebook'      => __( 'Facebook', Main::TEXT_DOMAIN ),
			'icon-spotify'       => __( 'Spotify', Main::TEXT_DOMAIN ),
			'icon-apple'         => __( 'Apple Music', Main::TEXT_DOMAIN ),
			'icon-instagram'     => __( 'Instagram', Main::TEXT_DOMAIN ),
			'icon-soundcloud'    => __( 'Soundcloud', Main::TEXT_DOMAIN ),
			'icon-bandcamp'      => __( 'Bandcamp', Main::TEXT_DOMAIN ),
			'icon-youtube-play'  => __( 'Youtube', Main::TEXT_DOMAIN ),
			'icon-twitter'       => __( 'Twitter', Main::TEXT_DOMAIN ),
			'icon-vimeo-squared' => __( 'Vimeo', Main::TEXT_DOMAIN ),
			'icon-link'          => __( 'Link', Main::TEXT_DOMAIN ),
		];

		return $icons;
	}
}
