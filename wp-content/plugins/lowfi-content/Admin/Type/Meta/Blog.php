<?php

namespace Lowfi\Content\Admin\Type\Meta;

use Lowfi\Content\Admin\Type\Meta\Helpers;
use Lowfi\Content\Admin\Type\Meta\Helpers\Icons;
use Lowfi\Content\Main;

class Blog extends Meta_Page {

	const PREFIX = 'blog_';

	public function __construct() {
		add_action( 'cmb2_init', [ $this, 'create_fields' ] );
	}

	public function create_fields() {
		$this->create_categories_box();
	}

	public function display_box_on_condition() {
		$blog = intval( get_option( 'page_for_posts' ) );

		return $blog == get_the_ID();
	}

	private function create_categories_box() {
		$box = $this->create_box( self::PREFIX . 'categories_', __( 'Categories', Main::TEXT_DOMAIN ) );

		$box->add_field( [
			'name'            => __( 'Categories shortcode', Main::TEXT_DOMAIN ),
			'id'              => 'shortcode',
			'type'            => 'text',
		] );

	}
}
