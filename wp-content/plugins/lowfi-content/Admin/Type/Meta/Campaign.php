<?php

namespace Lowfi\Content\Admin\Type\Meta;

use Lowfi\Content\Admin\Helpers\Constants;
use Lowfi\Content\Admin\Type\Meta\Helpers;
use Lowfi\Content\Admin\Type\Meta\Helpers\Icons;
use Lowfi\Content\Main;

class Campaign extends Meta_Page {

	const PREFIX = 'campaign_';

	public function __construct() {
		add_action( 'cmb2_init', [ $this, 'create_fields' ] );
	}

	public function create_fields() {
		$this->create_info_box();
	}

	public function display_box_on_condition() {
		return get_post_type( get_the_ID() ) === 'campaign';
	}

	private function create_info_box() {
		$box = $this->create_box( self::PREFIX . 'info_', __( 'Campaign Information', Main::TEXT_DOMAIN ) );

		$box->add_field( [
			'name'            => __( 'Artist', Main::TEXT_DOMAIN ),
			'id'              => 'artist',
			'type'            => 'text_medium',
		] );

		$box->add_field( [
			'name'            => __( 'Product Title', Main::TEXT_DOMAIN ),
			'id'              => 'title',
			'type'            => 'text_medium',
		] );

		$box->add_field( [
			'name'            => __( 'Image', Main::TEXT_DOMAIN ),
			'id'              => 'image',
			'type'            => 'file',
		] );

		$box->add_field( [
			'name'            => __( 'Video', Main::TEXT_DOMAIN ),
			'id'              => 'video',
			'type'            => 'file',
		] );

		$box->add_field( [
			'name'            => __( 'Background', Main::TEXT_DOMAIN ),
			'id'              => 'background',
			'type'            => 'file',
		] );

		$box->add_field( [
			'name'            => __( 'Background Size', Main::TEXT_DOMAIN ),
			'id'              => 'background_size',
			'type'            => 'select',
			'default'         => 'contain',
			'options'         => [
				'contain' => __( 'Contain', Main::TEXT_DOMAIN ),
				'cover' =>   __( 'Cover', Main::TEXT_DOMAIN ),
			],
		] );

		$box->add_field( [
			'name'            => __( 'Color Scheme', Main::TEXT_DOMAIN ),
			'id'              => 'color_scheme',
			'type'            => 'select',
			'show_option_none' => FALSE,
			'default'         => 'light',
			'options'         => [
				'light' => __( 'Light Background', Main::TEXT_DOMAIN ),
				'dark'  => __( 'Dark Background', Main::TEXT_DOMAIN ),
			],
		] );

		$group = $box->create_group( 'cta', '', 'Call-to-action' );

		$group->add_field( [
			'name'            => __( 'CTA Label', Main::TEXT_DOMAIN ),
			'id'              => 'cta_label',
			'type'            => 'text_medium',
		] );

		$group->add_field( [
			'name'            => __( 'CTA URL', Main::TEXT_DOMAIN ),
			'id'              => 'cta_url',
			'type'            => 'text_medium',
		] );

		$group->add_field( [
			'name'            => __( 'Open in a new window', Main::TEXT_DOMAIN ),
			'id'              => 'target',
			'type'            => 'checkbox',
		] );

		$group->add_field( [
			'name'            => __( 'Icon', Main::TEXT_DOMAIN ),
			'id'              => 'icon',
			'type'            => 'select',
			'options'         => Icons::get_options(),
		] );

		$box->add_field( [
			'name'            => __( 'Spotify Pre-Save Button', Main::TEXT_DOMAIN ),
			'id'              => 'spotify_presave',
			'type'            => 'checkbox',
		] );

		$box->add_field( [
			'name'            => __( 'Spotify Pre-Save CTA Label', Main::TEXT_DOMAIN ),
			'id'              => 'spotify_presave_label',
			'type'            => 'text_medium',
			'attributes'      => [
				'data-conditional-id' => 'spotify_presave',
			],
		] );

		$box->add_field( [
			'name'            => __( 'Spotify Track ID', Main::TEXT_DOMAIN ),
			'id'              => 'spotify_track_id',
			'type'            => 'text_medium',
			'attributes'      => [
				'data-conditional-id' => 'spotify_presave',
			],
		] );

		$box->add_field( [
			'name'            => __( 'Spotify Artist ID', Main::TEXT_DOMAIN ),
			'id'              => 'spotify_artist_id',
			'type'            => 'text_medium',
			'attributes'      => [
				'data-conditional-id' => 'spotify_presave',
			],
		] );

		$box->add_field( [
			'name'            => __( 'Release Date & Time', Main::TEXT_DOMAIN ),
			'id'              => 'spotify_release',
			'type'            => 'text_datetime_timestamp',
			'attributes'      => [
				'data-conditional-id' => 'spotify_presave',
			],
		] );

		$box->add_field( [
			'name'            => __( 'Follow Text', Main::TEXT_DOMAIN ),
			'id'              => 'links_text',
			'type'            => 'text_medium',
		] );

		$group = $box->create_group( 'follow', '', 'link' );

		$group->add_field( [
			'name'            => __( 'Icon', Main::TEXT_DOMAIN ),
			'id'              => 'icon',
			'type'            => 'select',
			'options'         => Icons::get_options(),
		] );

		$group->add_field( [
			'name'            => __( 'Link URL', Main::TEXT_DOMAIN ),
			'id'              => 'link_url',
			'type'            => 'text_url',
		] );

		$group = $box->create_group( 'content', '', 'content' );

		$group->add_field( [
			'name'            => __( 'Title', Main::TEXT_DOMAIN ),
			'id'              => 'title',
			'type'            => 'text_medium',
		] );

		$group->add_field( [
			'name'            => __( 'Text', Main::TEXT_DOMAIN ),
			'id'              => 'text',
			'type'            => 'textarea_small',
		] );

		$group->add_field( [
			'name'            => __( 'Link', Main::TEXT_DOMAIN ),
			'id'              => 'link',
			'type'            => 'text_url',
		] );

		$group->add_field( [
			'name'            => __( 'Image', Main::TEXT_DOMAIN ),
			'id'              => 'image',
			'type'            => 'file',
		] );

		$box->add_field( [
			'name'            => __( 'Media Section Title', Main::TEXT_DOMAIN ),
			'id'              => 'media_title',
			'type'            => 'text_medium',
		] );

		$box->add_field( [
			'name'            => __( 'Media Section Text', Main::TEXT_DOMAIN ),
			'id'              => 'media_text',
			'type'            => 'textarea_small',
		] );

		$box->add_field( [
			'name'            => __( 'Youtube Embed', Main::TEXT_DOMAIN ),
			'id'              => 'youtube_embed',
			'type'            => 'textarea_code',
		] );

		$box->add_field( [
			'name'            => __( 'Spotify Embed', Main::TEXT_DOMAIN ),
			'id'              => 'spotify_embed',
			'type'            => 'textarea_code',
		] );

		$box->add_field( [
			'name'            => __( 'Custom CSS', Main::TEXT_DOMAIN ),
			'id'              => 'custom_css',
			'type'            => 'textarea_code',
		] );
	}
}
