<?php

namespace Lowfi\Content\Admin\Type\Meta;

use Lowfi\Content\Admin\Helpers\Constants;
use Lowfi\Content\Admin\Type\Meta\Helpers;
use Lowfi\Content\Admin\Type\Meta\Helpers\Icons;
use Lowfi\Content\Main;

class Product extends Meta_Page {

	const PREFIX = 'product_';

	public function __construct() {
		add_action( 'cmb2_init', [ $this, 'create_fields' ] );
	}

	public function create_fields() {
		$this->create_info_box();
	}

	public function display_box_on_condition() {
		return get_post_type( get_the_ID() ) === 'product';
	}

	private function create_info_box() {
		$box = $this->create_box( self::PREFIX . 'info_', __( 'Product Information', Main::TEXT_DOMAIN ) );

		$box->add_field( [
			'name'            => __( 'Hold', Main::TEXT_DOMAIN ),
			'id'              => 'group',
			'type'            => 'text_medium',
		] );

		$box->add_field( [
			'name'            => __( 'Dato', Main::TEXT_DOMAIN ),
			'id'              => 'date',
			'type'            => 'text_date',
			'date_format'     => 'd/m/Y',
		] );

		$box->add_field( [
			'name'            => __( 'Tidspunkt', Main::TEXT_DOMAIN ),
			'id'              => 'time',
			'type'            => 'text_medium',
		] );

		$box->add_field( [
			'name'            => __( 'Sted', Main::TEXT_DOMAIN ),
			'id'              => 'location',
			'type'            => 'text_medium',
		] );

		$box->add_field( [
			'name'            => __( 'Underviser', Main::TEXT_DOMAIN ),
			'id'              => 'teacher',
			'type'            => 'text_medium',
		] );
	}
}
