<?php

namespace Lowfi\Content\Frontend\Type;

use Lowfi\Content\Main;
use Lowfi\Content\Admin\Type\Meta;
use Lowfi\Content\Frontend\Data\Data_Holder_Factory;

class Blog implements Page {

	/** @var Data_Holder_Factory */
	private $factory;

	public function matches_current_page() {
		return is_front_page() || is_home() || is_archive() || is_search();
	}

	public function init() {
		$this->factory = new Data_Holder_Factory( get_option( 'page_for_posts' ) );
		add_action( THEMEDOMAIN . '-main_content', [ $this, 'grid' ] );
		// add_action( THEMEDOMAIN . '-main_content', [ $this, 'cta' ] );
	}

	public function grid() {
		$data = $this->factory->create_data_holder( Meta\Blog::PREFIX . 'categories_' );

		if ( is_front_page() || is_home() ) {
			$title      = __( 'What\'s new', Main::TEXT_DOMAIN );
			$shortcode  = do_shortcode( '[latest-posts number="' . get_option( 'posts_per_page' ) . '"]' );
			$categories = get_categories();

			Main::get_template_part( 'Partials/Blog.php', [
				'title'      => $title,
				'data'       => $data,
				'categories' => do_shortcode( $data['shortcode'] ),
				'grid'       => $shortcode,
				'pagination' => $this->get_pagination(),
			] );
		}

		if ( is_archive() ) {
			$title      = $this->get_archive_title( get_the_archive_title() );
			$shortcode  = do_shortcode( '[latest-posts number="' . get_option( 'posts_per_page' ) . '" category="' . get_queried_object()->slug . '"]' );

			Main::get_template_part( 'Partials/Blog.php', [
				'title'      => $title,
				'categories' => '',
				'grid'       => $shortcode,
				'pagination' => $this->get_pagination(),
			] );
		}

		if ( is_search() ) {
			$title      = get_search_query();
			$shortcode  = do_shortcode( '[latest-posts number="' . get_option( 'posts_per_page' ) . '" category="' . get_queried_object()->slug . '"]' );

			Main::get_template_part( 'Partials/Search.php', [
				'title'      => $title,
				'categories' => '',
				'grid'       => get_the_content(),
				'pagination' => $this->get_pagination(),
			] );
		}
	}

	public function get_pagination() {
		$args = [
			'type'      => 'list',
			'prev_text' => __( '<', Main::TEXT_DOMAIN ),
			'next_text' => __( '>', Main::TEXT_DOMAIN ),
		];

		$list = paginate_links( $args );

		$pagination = sprintf( '<div class="pagination">%s</div>', $list );

		return $pagination;
	}

	function get_archive_title( $title ) {
	    if ( is_category() ) {
	        $title = single_cat_title( '', false );
	    } elseif ( is_tag() ) {
	        $title = single_tag_title( '', false );
	    } elseif ( is_author() ) {
	        $title = '<span class="vcard">' . get_the_author() . '</span>';
	    } elseif ( is_post_type_archive() ) {
	        $title = post_type_archive_title( '', false );
	    } elseif ( is_tax() ) {
	        $title = single_term_title( '', false );
	    }

	    return $title;
	}

	public function cta() {
		Main::get_template_part( 'Partials/CTA.php', [
			'headline' => cmb2_get_option( 'cta_options', 'headline' ),
			'subheadline' => cmb2_get_option( 'cta_options', 'subheadline' ),
			'image' => cmb2_get_option( 'cta_options', 'image' ),
			'cta_label' => cmb2_get_option( 'cta_options', 'cta_label' ),
			'cta_target' => cmb2_get_option( 'cta_options', 'cta_target' ),
		] );
	}
}
