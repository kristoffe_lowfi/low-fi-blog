<?php

namespace Lowfi\Content\Frontend\Type;

use Lowfi\Content\Main;
use Lowfi\Content\Core\Shortcode\Latest_Posts as Posts;

class Post implements Page {

	public function matches_current_page() {
		return ( is_single() || is_page() );
	}

	public function init() {
		add_action( THEMEDOMAIN . '-main_content', [ $this, 'content' ] );
		add_action( THEMEDOMAIN . '-main_content', [ $this, 'related' ] );

		if ( is_single() ) {
			// add_action( THEMEDOMAIN . '-after_main_content', [ $this, 'cta' ] );
		}
	}

	public function content() {
		while ( have_posts() ) {
			the_post();

			Main::get_template_part( 'Partials/Post.php', [
				'title'     => get_the_title(),
				'date'      => get_the_date( 'dS F Y' ),
				'image_url' =>  get_the_post_thumbnail_url( get_the_ID(), 'post-normal' ),
				'content'   =>  apply_filters( 'the_content', get_the_content() ),
			] );
		}
	}

	public function related() {
		Main::get_template_part( 'Partials/Related.php', [
			'title'   => __( 'Other stories', Main::TEXT_DOMAIN ),
			'related' => do_shortcode( '[latest-posts]' ),
		] );
	}
}
