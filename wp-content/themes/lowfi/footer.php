<?php
/**
 * Displays the footer.
 *
 * @link http://codex.wordpress.org/Stepping_into_Templates#Basic_Template_Files
 * @package WordPress
 *
 */
?>
	<footer class="footer" role="content-info">

		<div class="inner-grid">
			<?php if ( is_active_sidebar( 'sidebar-footer' ) ) : ?>
				<ul id="sidebar">
        			<?php dynamic_sidebar( 'sidebar-footer' ); ?>
				</ul>
			<?php endif; ?>
		</div>

		<div class="copyright">
			<div class="inner-grid">
				<div id="footer-logo">
					<a href="<?php echo get_home_url(); ?>">
						<?php the_custom_header_markup(); ?>
					</a>

					© <?php echo date( 'Y' ); ?> <?php echo bloginfo( 'name' ); ?>
				</div>

				<?php if ( is_active_sidebar( 'social-footer' ) ) : ?>
					<ul id="social">
	        			<?php dynamic_sidebar( 'social-footer' ); ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>

	</footer>

	<?php wp_footer(); ?>

</body>

</html>
