<?php
/**
 * Hooks for the frontend
 *
 * @package WordPress
 *
 *
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference
 * @link http://codex.wordpress.org/Plugin_API/Action_Reference
 */

namespace Lowfi;

/**
 * Loads css in the head and js at the end of body
 */
function enqueue() {

	if ( defined( 'WP_DEBUG' ) && WP_DEBUG && file_exists( get_stylesheet_directory() . '/assets/dist/main.css' ) ) {
		$suffix = '.';
	} else {
		$suffix = '.min.';
	}

	// Register stylesheet
	wp_register_style( 'main', THEME_URI . '/assets/dist/main' . $suffix . 'css', [], filemtime( get_stylesheet_directory() . '/assets/dist/main' . $suffix . 'css' ), FALSE );
	wp_enqueue_style( 'main' );

	// Adding scripts in the footer
	wp_register_script( 'scripts', THEME_URI . '/assets/dist/scripts.min.js', [ 'jquery' ], filemtime( get_stylesheet_directory() . '/assets/dist/scripts' . $suffix . 'js' ), TRUE );
	wp_enqueue_script( 'scripts' );

	// Inbuilt comment reply
	if ( is_singular() && comments_open() && ( get_option( 'thread_comments' ) == 1 ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueue', 1 );


function enqueue_styles() {
    wp_register_style( 'fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/solid.min.css' );
    wp_enqueue_style( 'fontawesome' );

	wp_enqueue_script( 'ajax-script', THEME_URI . '/assets/dist/scripts.min.js', array('jquery') );
	wp_localize_script( 'ajax-script', 'lowfi_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueue_styles' );


/**
 * Avoids returning random page if empty search string
 */
function search_filter( $query_vars ) {
	if ( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
		$query_vars['s'] = ' ';
	}

	return $query_vars;
}

add_filter( 'request', __NAMESPACE__ . '\search_filter' );


/**
 * Add taxonomy class to the body tag
 */
function body_classes( $classes ) {
	global $post;

	if ( function_exists( 'pll_current_language' ) ) {
		$classes[] = sprintf( 'lang-%s', pll_current_language( 'slug' ) );
	}

	if ( isset( $post ) && isset( $post->post_type ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}

	if ( is_tax() ) {
		$classes[] = 'taxonomy';
	}

	 if ( is_user_logged_in() ) {
		$classes[] = 'logged-in';
	 }

	return $classes;
}

add_filter( 'body_class', __NAMESPACE__ . '\body_classes' );


/**
 * Deregister unused scripts.
 */
function deregister_scripts() {
	// WP Emoji
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	// Remove wp-embed script
	remove_action( 'rest_api_init', 'wp_oembed_register_route' );
	remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	remove_action( 'wp_head', 'wp_oembed_add_host_js' );

	// Plugins
	wp_dequeue_script( 'optinmonster-api-script' );
}

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\deregister_scripts', 100 );


/**
 * Google Tag Manager Script
 */
function header_gtm_script() {
	?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PW9MNTB');</script>
	<!-- End Google Tag Manager -->
	<?php
}

add_action( 'wp_head', __NAMESPACE__ . '\header_gtm_script' );

/**
 * Google Tag Manager Script
 */
function header_gtm_no_script() {
	?>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PW9MNTB"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<?php
}

add_action( THEMEDOMAIN . '-before_main_nav' , __NAMESPACE__ . '\header_gtm_no_script' );


/**
 * Google Analytics Script
 */
function header_ga_script() {
	?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-FG01KBEW85"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'G-FG01KBEW85');
	</script>
	<?php
}

// add_action( 'wp_head', __NAMESPACE__ . '\header_ga_script' );


/**
 * Facebook Script
 */
function header_fb_script() {
	?>
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '302721421222664');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=302721421222664&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	<?php
}

// add_action( 'wp_head', __NAMESPACE__ . '\header_fb_script' );

/**
 * Hotjar Script
 */
function header_hotjar_script() {
	?>
	<!-- Hotjar Tracking Code for https://lowfi.dk/ -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:2372273,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	<?php
}

// add_action( 'wp_head', __NAMESPACE__ . '\header_hotjar_script' );


/**
 * Convertflow Script
 */
function header_convertflow_script() {
	?>
	<script async src="https://js.convertflow.co/production/websites/25266.js"></script>
	<?php
}

// add_action( 'wp_head', __NAMESPACE__ . '\header_convertflow_script' );

/**
 * FB Business Verificaiton
 */
function header_fb_bm_script() {
	?>
	<meta name="facebook-domain-verification" content="xaicgz6jm321yle96f72a0ia7utrlq" />
	<?php
}

// add_action( 'wp_head', __NAMESPACE__ . '\header_fb_bm_script' );


function placeholder_image( $src ) {
	$src = THEME_URI . '/assets/img/sa-shop-placeholder.png';
	return $src;
}

// add_filter( 'woocommerce_placeholder_img_src', __NAMESPACE__ . '\placeholder_image' );


/**
 * Add mime types support.
 */
function add_mimes_types( $existing_mimes = [] ) {

	$existing_mimes = [
		'jpg|jpeg|jpe' => 'image/jpeg',
		'gif'          => 'image/gif',
		'png'          => 'image/png',
		'bmp'          => 'image/bmp',
		'tif|tiff'     => 'image/tiff',
		'ai'           => 'application/postscript',
		'eps'          => 'application/postscript',
		'psd'          => 'image/vnd.adobe.photoshop',
		'pdf'          => 'application/pdf',
		'svg'          => 'image/svg+xml',
		'svg'          => 'image/png',
		'zip'          => 'application/zip',
		'gz'           => 'application/x-gzip',
	];

	return $existing_mimes;
}

add_filter( 'upload_mimes', __NAMESPACE__ . '\add_mimes_types' );


/**
 * Exclude category from WC Shop
 */
function custom_pre_get_posts_query( $q ) {
	$tax_query = (array) $q->get( 'tax_query' );

	$tax_query[] = [
		'taxonomy' => 'product_cat',
		'field'    => 'ID',
		'terms'    => [ 25 ],
		'operator' => 'NOT IN'
	];

	$q->set( 'tax_query', $tax_query );
}

add_action( 'woocommerce_product_query', __NAMESPACE__ . '\custom_pre_get_posts_query' );


/**
 * Disable admin bar for all users except admin
 */
function remove_admin_bar() {
	if ( ! current_user_can( 'administrator' ) && ! is_admin() ) {
	  show_admin_bar( FALSE );
	}
}

add_action( 'after_setup_theme', __NAMESPACE__ . '\remove_admin_bar' );


/**
 * Disable admin bar bump
 */
function remove_admin_login_header() {
    remove_action( 'wp_head', '_admin_bar_bump_cb' );
}
add_action( 'get_header', __NAMESPACE__ . '\remove_admin_login_header' );
